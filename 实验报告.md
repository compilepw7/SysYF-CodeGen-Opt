
### 贡献
+ 龚乐： 33.3%
+ 武汉： 33.3%
+ 赵卓然： 33.3%
### Part1

part1部分主要可以分为两步，分别是可用表达式分析和公共子表达式删除，下面分别简述具体步骤

#### 可用表达式分析

定义`SubExpr`类，表达一个表达式

```c++
class SubExpr
{
public:
    int Optype;//类型
    Value *op1, *op2, *ins;//右值左边操作数、右值右边操作数、左值

    SubExpr(Instruction *i)//构造函数，根据指令构造
    {
        ins = i;
        if (i->is_add())
            Optype = 0;
        else if (i->is_mul())
            Optype = 1;
        else if (i->is_sub())
            Optype = 2;
        else if (i->is_div())
            Optype = 3;
        else if (i->is_rem())
            Optype = 4;
        else
        {
            std::cout << "Unexpected type when handling subexpr." << std::endl;
            abort();
        }
        op1 = i->get_operand(0);
        op2 = i->get_operand(1);
    }
    bool operator==(const SubExpr a)//重载相等运算符
    {
        if (a.ins == this->ins)
            return true;
        if (a.Optype != this->Optype)
            return false;
        if (a.Optype == 0 || a.Optype == 1)
            return ((a.op1 == this->op1 && a.op2 == this->op2) || (a.op1 == this->op2 && a.op2 == this->op1));
        else
            return (a.op1 == this->op1 && a.op2 == this->op2);
    }
};
```

```c++
class BitVector//实现了一个虚假的bitvector
{
public:
    typedef bool BitValType;
    BitVector()
        : BitVector(0)
    {
    }
    BitVector(int size, bool init_fill = true)//构造函数，传入大小和初始0/1
    {
        int n = size;
        this->data_ = new int[n];
        if (!init_fill)
        {
            for (int i = 0; i < n; i++)
            {
                this->data_[i] = 0;
            }
        }
        else
        {
            for (int i = 0; i < n; i++)
            {
                this->data_[i] = 1;
            }
        }
        this->size_ = size;
    }
    ~BitVector()
    {
    }
    int &get(int index)//获取index位置的0/1值
    {
        if (index < 0 || index > this->size_)
        {
            throw std::out_of_range("BitVector::get() parameter index");
        }
        return this->data_[index];
    }
    bool set(int index, const BitValType &val)//设置index位置的0/1值
    {
        if (index < 0 || index > this->size_)
        {
            throw std::out_of_range("BitVector::set() parameter index");
        }
        this->data_[index] = val;
        return true;
    }
    bool operator==(BitVector a)//重载相等
    {
        if (a.getsize() != this->getsize())
        {
            return false;
        }
        else
        {
            for (int i = 0; i < a.getsize(); i++)
            {
                if (a.get(i) != this->get(i))
                    return false;
            }
            return true;
        }
    }
    int getsize()//获取bitvector大小
    {
        return size_;
    }

private:
    int size_;
    int *data_;
};
```

```c++
class ComSubExprEli : public Pass
{
public:
    explicit ComSubExprEli(Module *module) : Pass(module) {}
    const std::string get_name() const override { return name; }
    void execute() override;
    static bool is_valid_expr(Instruction *inst);
    std::map<BasicBlock *, std::set<SubExpr *>> gen, kill;//每个块都有的gen和kill
    std::vector<SubExpr *> all;
    std::vector<Value *> def;
    std::map<BasicBlock *, BitVector> in, out, gen_e, kill_e;

private:
    const std::string name = "ComSubExprEli";
};
```

- 可用表达式算法思路
  - 计算每个基本块里的gen、kill集合
  - 遍历块中指令,把指令加入all，得到基本块里有定值的变量合集，存入def向量
  - 第二次遍历，如果该条指令所在基本块后面没有对变量定值，则将对应表达式加入gen集合并且将含有该条指令等号左边变量的表达式加入kill集合。
  - gen、kill转为bitvector，初始化in、out
  - 迭代计算in、out

#### 公共子表达式删除

- 算法思路（考虑SSA格式)
  - 对每个基本块，新建一个newlist用来说明在某步骤时可用的表达式
  - newlist初始为in集合里的表达式。
  - 对基本块里的指令b，如果在newlist里并且为a，全局替换b->a,之后删除b。
  - 如果不在newlist里，则加入newlist

**B1-1**.已如前述。

**B1-2**. 若要将call指令、load指令列入公共子表达式删除的考虑范围，除了你实现的公共子表达式删除算法，还需要考虑哪些其他因素？

- 需要将前继的load，store指令一同处理，用被load的变量替换临时变量的操作数，并且也要注意将load的变化暂存起来做对比判断是否可以删除公共表达式。
- call指令要判断调用参数是否发生了变化，并且和call内部函数的分析有关，就比较复杂了。（比如函数里调用系统urandom，导致每次结果都不同，这样显然不能进行公共子表达式删除）

### Part2

**B2-1**. 请说明你实现的活跃变量分析算法的设计思路。

- 首先逐语句计算def-use链，对不同类型指令分别处理。
- 对于除了phi指令之外的指令，如果左右操作数(如果有的话)不为常数，若无定义则设为被使用，在所有前驱块中被激活
- 对于phi指令，phi指令的操作数为成对的，两个两个判断，该操作数只被对应的来源块激活。
- 按照书上顺序迭代求解数据流方程，向后查找，若某个后继块有此块中被激活的变量在入口处活跃，则推入live_out中。
- 搜索所有当前块无定义且被使用的变量，若不在入口活跃，则推入live_in中，并标记数据流解有变化
- 搜索所有当前块出口活跃的变量，若此块无定义且不在入口活跃，则推入live_in中，并标记数据流解有变化，同时把此变量在所有前驱块中设为active

### Part3

- **任务3-1 构建支配树的算法**

  - **B3-1**. 证明：若 $x$ 和 $y$ 支配 $b$，则要么 $x$ 支配 $y$，要么 $y$ 支配 $x$。
  - 若不然，假如x不支配y并且y不支配x，不妨考虑x不支配y，说明存在至少一条通路m，使entry到y不经过x，又y支配b，合并上述可得至少存在一条经过y的通路，使得entry到b不经过x，也就是x不支配b，矛盾！
  - **B3-2**. 在 [dom.pdf](doc/dom.pdf) 中，`Figure 1: The Iterative Dominator Algorithm` 是用于计算支配关系的迭代算法，该算法的内层 `for` 循环是否一定要以后序遍历的逆序进行，为什么？
    - 不一定要用后序遍历的逆序进行，使用别的顺序也可以，但是收敛速率会更慢一点。一般来说对于前向数据流问题用后序遍历的逆序会比较方便，这个顺序在前向数据流问题中可以尽可能合理的安排各个结点的迭代顺序。如果CFG没有环的话，也就是DAG，使用逆后序可以通过一次迭代达到收敛（加上判断是两次）。当CFG存在循环的时候，逆后序这个概念在这里就没有意义了，但是“逆后序”也就是深度优先顺序还是可以让数据更快速的收敛。
  - **B3-3**. `Figure 3: The Engineered Algorithm` 为计算支配树的算法。在其上半部分的迭代计算中，内层的 `for` 循环是否一定要以后序遍历的逆序进行，为什么？
    - 一定要用后序遍历的逆序，因为这样才能保证前驱节点的IDom结点已经计算过了，不会出现未定义的状况。
  - **B3-4**. 在 [dom.pdf](doc/dom.pdf) 中，`Figure 3: The Engineered Algorithm` 为计算支配树的算法。其中下半部分 `intersect` 的作用是什么？内层的两个 `while` 循环中的小于号能否改成大于号？为什么？
    - ` It implements a “two-finger” algorithm – one can imagine a finger pointing to each dominator set, each finger moving independently as the comparisons dictate. In this case, the comparisons are on postorder numbers; for each intersection, we start the two fingers at the ends of the two sets, and, until the fingers point to the same postorder number, we move the finger pointing to the smaller number back one element. Remember that nodes higher in the dominator tree have higher postorder numbers, which is why intersect moves the finger whose value is less than the other finger’s. `
    - 这段话可以看出来
    - intersect函数：找这两个前驱的在DomTree中最近公共祖先。
    - 不能改成大于号，因为支配树中高的节点有更高的后序数，所以每次都移动小的那个节点。
  - **B3-5**. 这种通过构建支配树从而得到支配关系的算法相比教材中算法 9.6 在时间和空间上的优点是什么？
    - 教材中迭代数据流分析算法的时间复杂度是O($n^2$)，这种算法的时间复杂度也是O($n^2$)，但是在编译器使用的数据量范围内甚至比[Lengauer-Tarjan algorithm](http://delivery.acm.org/10.1145/360000/357071/p121-lengauer.pdf?ip=222.197.181.85&id=357071&acc=ACTIVE SERVICE&key=BF85BBA5741FDC6E.21AB2B2297141EDA.4D4702B0C3E38B35.4D4702B0C3E38B35&CFID=656466333&CFTOKEN=53941007&__acm__=1471399542_0cc5a60484e69e08716a5a77b923a0b8) 这种O(E𝛼(E,N))(近似线性)的算法更快（据论文所说）
    - 在空间上由于该方法并没有将每个结点的支配结点定义为全集并且不直接计算结点 ***n*** 的支配集合，所以比教材中迭代数据流分析算法好一点

- **任务3-2 理解支配树的实现**

  [DominateTree.cpp](src/Optimize/DominateTree.cpp) 和 [DominateTree.cpp](src/Optimize/RDominateTree.cpp) 是本实验框架提供的支配树和反向支配树的一种实现，请阅读代码并回答以下问题：

- **B3-6**. 在反向支配树的构建过程中，是怎么确定 EXIT 结点的？为什么不能以流图的最后一个基本块作为 EXIT 结点？

  - ```c++
    for(auto bb:f->get_basic_blocks()){
            auto terminate_instr = bb->get_terminator();
            if(terminate_instr->is_ret()){
                exit_block = bb;
                break;
            }
        }
        if(!exit_block){
            std::cerr << "exit block is null, function must have only one exit block with a ret instr\n";
            std::cerr << "err function:\n" << f->print() << std::endl;
            exit(1);
        }
    ```

  - 上述代码确定了exit节点，算法是最后一个包含有return指令的基本块

  - 流图的最后一个基本块可能包含了其他goto语句等，不一定是EXIT



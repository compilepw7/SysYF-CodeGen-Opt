#ifndef BAZINGA_COMPILER_LINEARSCAN_H
#define BAZINGA_COMPILER_LINEARSCAN_H

#include <queue>
#include <algorithm>
#include <iostream>
#include "LIR/LIRBuilder.h"
#include "LIR/LIRLabel.h"
#include "LIR/LIROperand.h"

class LIR_OpVisitState{
public:
    std::list<LIROperand *> input_oprs;
    std::list<LIROperand *> temp_oprs;
    std::list<LIROperand *> output_oprs;
    bool has_call;

    void visit(LIRInstruction * inst){
        temp_oprs.clear();
        output_oprs.clear();
        input_oprs.clear();
        if (inst->getResult() != nullptr && inst->getResult()->isVirtualRegTy()) {
            output_oprs.push_back(inst->getResult());
        }
        for(auto op : inst->getOperands())
        {
            if (op->isVirtualRegTy()) {
                input_oprs.push_back(op);
            }
        }
        if (auto br = dynamic_cast<LIRBranchInstruction *>(inst)) {
            has_call = br->isCall();
        } else {
            has_call = false;
        }
    }
};

class Range{
public:
    Range(int from, int to){
        this->from = from;
        this->to = to;
    }
    void set_from(int new_from){
        this->from = new_from;
    }
    void set_to(int new_to){
        this->to = new_to;
    }
    int from;
    int to;
};

class UsePosition{
public:
    UsePosition(int position, int use_kind){
        this->position = position;
        this->use_kind = use_kind;
    }
    int position;
    int use_kind;
};

class Interval{
public:
    Interval(int reg_num){
        if (reg_num == 0) {
            printf("DEBUG");
        }
        this->reg_num = reg_num;
    }
    Interval(int reg_num, LIRFunction *cur_fun){
        this->reg_num = reg_num;
        this->cur_fun = cur_fun;
    }
    Interval(int reg_num, int assigned_reg){
        this->reg_num = reg_num;
        this->assigned_reg = assigned_reg;
    }
    Interval(int reg_num, int assigned_reg, LIRFunction *cur_fun){
        this->reg_num = reg_num;
        this->assigned_reg = assigned_reg;
        this->cur_fun = cur_fun;
    }
    int get_first_use_position(){
        int first_use = INT32_MAX;
        for(auto use: this->UsePositionList){
            if(use->position < first_use){
                first_use = use->position;
            }
        }
        assert(first_use>=this->ranges.front()->from);
        return first_use;
    }
    int get_last_use_position(){
        int last_use = -1;
        if(!this->UsePositionList.empty()){
            for(auto use: this->UsePositionList){
                last_use = std::max(last_use, use->position);
            }
        }
        return last_use;
    }
    void set_cur_fun(LIRFunction *cur_fun_){
        this->cur_fun = cur_fun_;
    }
    int get_reg_num(){
        return this->reg_num;
    }
    bool have_parent(){
        if(this->split_parent== nullptr)
            return false;
        return true;
    }
    Interval *get_near(){
        int  pos = this->get_range().front()->from;
        auto parent_interval = this->split_parent;
        int nearist = parent_interval->ranges.back()->to;
        Interval* ret = parent_interval;
        assert(nearist<=pos);
        for(auto tmp : parent_interval->split_children){
            int tmp_pos = tmp->ranges.back()->to;
            if(nearist < tmp_pos && tmp_pos<= pos){
                nearist = tmp_pos;
                ret = tmp;
            }
        }
        return ret;
    }
    Interval *get_brother(int pos){
        auto parent_interval = this->split_parent;
        int nearist = parent_interval->ranges.back()->to;
        Interval* ret = parent_interval;
        assert(nearist<=pos);
        for(auto tmp : parent_interval->split_children){
            int tmp_pos = tmp->ranges.back()->to;
            if(nearist < tmp_pos && tmp_pos<= pos && tmp->get_assigned_reg()> -1){
                nearist = tmp_pos;
                ret = tmp;
            }
        }
        return ret;
    }
    Interval *get_stack(int pos){
        auto parent_interval = this->split_parent;
        int nearist = parent_interval->ranges.back()->to;
        Interval* ret = parent_interval;
        assert(nearist<=pos);
        for(auto tmp : parent_interval->split_children){
            int tmp_pos = tmp->ranges.back()->to;
            if(nearist < tmp_pos && tmp_pos<= pos && tmp->getSpillId()> -1){
                nearist = tmp_pos;
                ret = tmp;
            }
        }
        return ret;
    }
    LIRFunction *get_cur_fun(){
        return this->cur_fun;
    }
    void set_assigned_reg(int reg){
        this->assigned_reg = reg;
    }
    void set_split_parent(Interval *interval){
        this->split_parent = interval;
    }
    void add_split_child(Interval *child){
        this->split_children.push_back(child);
    }
    void add_range(int from, int to){
        this->sort_ranges();
        int start = from;
        int end = to;
        auto temp_range = new Range(start, to);
        std::vector<Range *> wait_delete_range;
        bool changed = true;
        while(changed){
            changed = false;
            wait_delete_range.clear();
            for(auto ra: this->ranges){
                if(range_intersect(ra, temp_range)){
                    start = std::min(start, ra->from);
                    end = std::max(end, ra->to);
                    temp_range->from = start;
                    temp_range->to = end;
                    wait_delete_range.push_back(ra);
                    changed = true;
                }
                else if(ra->from == end || ra->to == start){
                    // merge [a, b] [b, c] to [a, c]
                    start = std::min(start, ra->from);
                    end = std::max(end, ra->to);
                    temp_range->from = start;
                    temp_range->to = end;
                    wait_delete_range.push_back(ra);
                    changed = true;
                }
            }
            for(auto x: wait_delete_range){
                this->ranges.remove(x);
            }
        }
        auto new_range = new Range(start, end);
        this->ranges.push_back(new_range);
        this->sort_ranges();
    }
    void add_range(Range *range){
        this->sort_ranges();
        int start = range->from;
        int end = range->to;
        std::vector<Range *> wait_delete_range;
        bool changed = true;
        while(changed){
            changed = false;
            wait_delete_range.clear();
            for(auto ra: this->ranges){
                if(range_intersect(ra, range)){
                    start = std::min(start, ra->from);
                    end = std::max(end, ra->to);
                    range->from = start;
                    range->to = end;
                    wait_delete_range.push_back(ra);
                    changed = true;
                }
                else if(ra->from == end || ra->to == start){
                    // merge [a, b] [b, c] to [a, c]
                    start = std::min(start, ra->from);
                    end = std::max(end, ra->to);
                    range->from = start;
                    range->to = end;
                    wait_delete_range.push_back(ra);
                    changed = true;
                }
            }
            for(auto x: wait_delete_range){
                this->ranges.remove(x);
            }
        }
        auto new_range = new Range(start, end);
        this->ranges.push_back(new_range);
        this->sort_ranges();
    }
    void remove_range(Range *range){
        ranges.remove(range);
        this->sort_ranges();
    }
    void add_use_pos(int pos, int kind){
        auto new_use_pos = new UsePosition(pos, kind);
        UsePositionList.push_back(new_use_pos);
    }
    void add_use_pos(UsePosition *use_position){
        UsePositionList.push_back(use_position);
    }

    bool is_family(Interval *interval){
        if(interval== nullptr){
            return false;
        }
        if(interval->have_parent()&&this->have_parent()){
            if(this->split_parent==interval->split_parent)
                return true;
        }
        else if(interval->have_parent() && !this->have_parent()){
            if(interval->split_parent==this)
                return true;
        }
        else if(this->have_parent() && !interval->have_parent()){
            if(this->split_parent==interval)
                return true;
        }
        else{
            if(this==interval)
                return true;
        }
        return false;
    }

    std::list<Range *> get_range(){
        return this->ranges;
    }
    Range* get_range(int pos){
        for (auto ret:ranges){
            if (ret->from<=pos && ret->to >= pos){
                return ret;
            }
        }
        return nullptr;
    }
    int get_next_use_position(int position){
        int ret = INT32_MAX;
        for(auto use: this->UsePositionList){
            if(use->position >= position){
                ret = std::min(ret, use->position);
            }
        }
        return ret;
    }
    Interval *split(int split_pos){
        auto new_vir_reg = LIRVirtualRegister::create();
        LIRVirtualRegister::getVReg(this->reg_num)->replaceAllUseAfterID(new_vir_reg, split_pos);
        auto new_interval = new Interval(new_vir_reg->getRegisterID());
        std::vector<Range *> wait_delete_range;
        for(auto range: this->ranges){
            if(range->to <= split_pos){
                continue;
            }
            else if(range->from >= split_pos){
                wait_delete_range.push_back(range);
                auto new_range = new Range(range->from, range->to);
                new_interval->add_range(new_range);
            }
            else{
                auto new_range = new Range(split_pos, range->to);
                assert(split_pos <= range->to);
                assert(range->from <= split_pos);
                range->to = split_pos;
                new_interval->add_range(new_range);
            }
        }
        for(auto x: wait_delete_range){
            this->remove_range(x);
        }
        this->sort_ranges();
        new_interval->sort_ranges();
        std::vector<UsePosition *> wait_delete_usepos;
        for(auto use_pos: this->UsePositionList){
            if(use_pos->position >= split_pos){
                wait_delete_usepos.push_back(use_pos);
                auto new_use_pos = new UsePosition(use_pos->position, use_pos->use_kind);
                new_interval->UsePositionList.push_back(new_use_pos);
            }
            else {
                continue;
            }
        }
        for(auto x: wait_delete_usepos){
            this->UsePositionList.remove(x);
        }
        if(new_interval->ranges.empty()){
            return nullptr;
        }
        this->add_split_child(new_interval);
        if(this->split_parent== nullptr)
        {
            this->add_split_child(new_interval);
            new_interval->set_split_parent(this);
        }
        else
        {
            new_interval->set_split_parent(this->split_parent);
            this->split_parent->add_split_child(new_interval);
        }
        assert(!ranges.empty() && "Got an empty range interval!");
        return new_interval;
    }
    bool covers(int op_id){
        for(auto range: this->ranges){
            if(range->from <= op_id && range->to > op_id) {
                return true;
            }
        }
        return false;
    }
    bool intersects(Interval *interval){
        for(auto range: this->ranges){
            for(auto range_: interval->ranges){
                if(range->from < range_->to && range->to > range_->from){
                    return true;
                }
            }
        }
        return false;
    }
    bool range_intersect(Range *r1, Range *r2){
        if(r1->from < r2->to && r1->to > r2->from){
            return true;
        }
        return false;
    }
    int next_intersection(Interval *interval, int pos){
        for(auto range: this->ranges){
            for(auto range_: interval->ranges){
                if(range->from < range_->to && range->to > range_->from){
                    int intersection=std::max(range->from,range_->from);
                    if (intersection >= pos){
                        return intersection;
                    }
                }
            }
        }
        return INT32_MAX;
    }
    Interval *child_at(int op_id){
        //if (covers(op_id)) return this;
        Interval * parent;
        if(this->have_parent()) {
            parent = this->split_parent;
        } else
        {
            parent = this;
        }
        for(auto child: parent->split_children){
            for(auto tmp : child->UsePositionList){
                if(tmp->position==op_id)
                    return child;
            }
            if(child->covers(op_id)){
                return child;
            }
        }
        return this;
    }
    int get_assigned_reg() const{
        return assigned_reg;
    }

    int get_usekind(int pos){
        for(auto usepos : UsePositionList)
            if(pos == usepos->position)
                return usepos->use_kind;
        return 0;
    }

    Range * get_next_use(int pos){
        for(auto tmp : ranges){
            if(pos < tmp->from)
                return tmp;
        }
        return nullptr;
    }

    int getNextUsePos(int pos) {
        int ret = INT32_MAX;
        for (auto U: UsePositionList) {
            if (U->position > pos && U->position < ret) {
                ret = U->position;
            }
        }
        return ret;
    }

    int getOriginVReg() {
        auto inv = this;
        while (inv->have_parent()) {
            inv = inv->split_parent;
        }
        return inv->get_reg_num();
    }

    void setSpillId(int spillId){
        _spillId = spillId;
    }

    int getSpillId()
    {
        return _spillId;
    }

    LIRFunction *cur_fun = nullptr;


private:
    int _spillId = -1;
    int reg_num;
    int assigned_reg = -1;
    std::list<Range *> ranges;
    std::list<UsePosition *> UsePositionList;
    Interval *split_parent = nullptr;
    std::list<Interval *> split_children;
    Interval *register_hint = nullptr;
    void sort_ranges(){
        std::list<Range *> tmp_list;
        while(!ranges.empty()){
            Range *min_start_range = ranges.front();
            for(auto ra: ranges){
                if(ra->from < min_start_range->from
                   || ra->from == min_start_range->from && ra->to < min_start_range->to) min_start_range = ra;
            }
            tmp_list.push_back(min_start_range);
            ranges.remove(min_start_range);
        }
        ranges.clear();
        for(auto range: tmp_list){
            ranges.push_back(range);
        }
    }
    bool operator <(Interval *b){
        return this->ranges.front()->from < b->ranges.front()->from;
    }

public:
    std::string print() const {
        std::string ret = "RegNum:  ";
        ret += std::to_string(reg_num);
        ret += "\tAssignedReg:  ";
        ret += std::to_string(assigned_reg);
        ret += "\tSpillID:  ";
        ret += std::to_string(_spillId);
        for (auto rg: ranges) {
            ret += "  [";
            ret += std::to_string(rg->from);
            ret += ", ";
            ret += std::to_string(rg->to);
            ret += "]";
        }
        ret += "\n";
        return ret;
    }
};

class LinearScan {
private:
    struct cmp{
        bool operator () (Interval *temp1, Interval *temp2){
            return temp1->get_range().front()->from > temp2->get_range().front()->from;
        }
    };

    std::list<LIRInstruction *> range_move_instructions(std::list<LIRInstruction *> mov_list);
    std::priority_queue<Interval *, std::vector<Interval *>, cmp> unhandled;
    std::vector<Interval *> handled;
    std::vector<Interval *> active;
    std::vector<Interval *> inactive;
    LIRFunction *_currentFunction;
    std::list<LIRBasicBlock *> blocks;
    std::map<LIROperand*,Interval *>intervals;
    std::map<int ,LIRInstruction *>inst_map;
    std::map<LIRBasicBlock* ,int>BBfirst;
    std::map<LIRBasicBlock* ,int>BBLast;

    void addRangeOrCreate(LIROperand *opr, int from, int to);
    void addUsePosition(LIROperand *opr, int pos, int kind);

    void computeBlockOrder();
    void numberOperations();
    void computeLocalLiveSets();
    void computeGlobalLiveSets();
    void buildIntervals();
    void sillyWalkIntervals();
    bool sillyTryAllocateFreeReg(Interval *current, int pos);
    void sillyHandleBlocked(Interval *current);
    void sillyAssignRegNum();

    void walkIntervals();
    void resolveDataFlow();
    void assignRegNum();
    bool tryAllocateFreeReg(Interval *current, int pos);
    void allocateBlockedReg(Interval *current, int pos);
    void clearAll();

public:
    void linearScan(LIRFunction * func);
    void sillyLinearScan(LIRFunction *func);
};


class MoveResolver{
private:
    LIRBasicBlock *insert_block;
    LIRLabel *target;
    int insert_idx;
    bool flag = false;
    std::map<Interval *,Interval *> interval_map;
public:
    void add_mapping(Interval * from, Interval *to){
        interval_map[from] = to;
    }
    void find_insert_position(LIRBasicBlock* from, LIRBasicBlock* to){
        if(interval_map.empty()){
            return ;
        }
        auto cur_fun = from->getParent();
        auto new_bb = LIRBasicBlock::create(from->getName() + "_to_" + to->getName());
        cur_fun->addBasicBlock(new_bb);
        insert_block = new_bb;
        for(auto &instr: from->getInstructions()){
            if(auto br_instr = dynamic_cast<LIRBranchInstruction *>(instr)){
                if(br_instr->getTarget() == to){
                    insert_block->addInst(LIRBranchInstruction::create(to));
                    br_instr->setTarget(new_bb);
                    break;
                }
            }
        }
    }
    void resolve_mappings(){
        std::cout << "======== RESOLVE MAPPING DEBUG ========" << std::endl;
        if(interval_map.empty()){
            std::cout << "interval map is empty" << std::endl;
            return;
        }
        std::cout << "-------- BEFORE RESOLVE --------" << std::endl;
        std::vector<std::string> mov_resolve;
        std::vector<std::string> load_resolve;
        std::vector<std::string> store_resolve;
        std::vector<Interval *> stack_to_stack;
        for(auto pair: interval_map){
            auto from_interval = pair.first;
            auto to_interval = pair.second;
            if(from_interval->get_assigned_reg() == -1
               && to_interval->get_assigned_reg() == -1){
                stack_to_stack.push_back(from_interval);
            }
            else if(from_interval->get_assigned_reg() == to_interval->get_assigned_reg()
                    && from_interval->get_assigned_reg() != -1){
                stack_to_stack.push_back(from_interval);
            }
        }
        for(auto x: stack_to_stack){
            interval_map.erase(x);
        }
        for(auto pair: interval_map){
            auto from_interval = pair.first;
            auto to_interval = pair.second;
            if(to_interval->get_assigned_reg() == -1){
                std::cout << "store r" << from_interval->get_assigned_reg() << " to stack with spillID = " << to_interval->getSpillId() << std::endl;
            }
            else if(from_interval->get_assigned_reg() == -1){
                std::cout << "load r" << to_interval->get_assigned_reg() << " form stack with spillID = " << from_interval->getSpillId() << std::endl;
            }
            else{
                std::cout << "mov r" << from_interval->get_assigned_reg() << " to r" << to_interval->get_assigned_reg() << std::endl;
            }
        }
        bool changed = true;
        // record next insert instr position (after the insert_after_instr)
        LIRInstruction *insert_after_instr = nullptr;
        // first handle store and load instr
        std::vector<Interval *> wait_delete_interval;
        for(auto pair: interval_map){
            auto from_interval = pair.first;
            auto to_interval = pair.second;
            if(to_interval->get_assigned_reg() == -1) {
                if(to_interval->getSpillId()==16)
                    auto debug = -1;
                auto spill_id = to_interval->getSpillId();
                auto from_phi_reg = LIRPhysicsRegister::create(from_interval->get_assigned_reg());
                auto store_instr = LIRStoreInst::create(from_phi_reg, LIRPhysicsRegister::create(13),LIRConstantOperand::create(spill_id));
                auto a = "store r" + std::to_string(from_interval->get_assigned_reg()) + " to stack " + "with spillID " + std::to_string(spill_id);
                store_resolve.push_back(a);
                if(insert_after_instr == nullptr){
                    insert_block->addInstFront(store_instr);
                }
                else{
                    insert_block->addInstAfter(insert_after_instr, store_instr);
                }
                insert_after_instr = store_instr;
                wait_delete_interval.push_back(from_interval);
            }
            else if(from_interval->get_assigned_reg() == -1){
                auto spill_id = from_interval->getSpillId();
                auto to_phi_reg = LIRPhysicsRegister::create(to_interval->get_assigned_reg());
                auto load_instr = LIRLoadInst::create(to_phi_reg, LIRPhysicsRegister::create(13),LIRConstantOperand::create(spill_id));
                auto a = "load from stack to r" + std::to_string(to_interval->get_assigned_reg()) + "with spillID " + std::to_string(spill_id);
                load_resolve.push_back(a);
                insert_block->addInstBeforeBr(load_instr);
                wait_delete_interval.push_back(from_interval);
            }
        }
        for(auto x: wait_delete_interval){
            interval_map.erase(x);
        }
        // second handle remained mov instr (skip loop handling)
        std::map<int, int> mov_map;
        for(auto pair: interval_map){
            mov_map[pair.first->get_assigned_reg()] = pair.second->get_assigned_reg();
        }
        while(!interval_map.empty() && changed) {
            changed = false;
            for(auto pair: interval_map){
                auto from_interval = pair.first;
                int from_interval_reg = from_interval->get_assigned_reg();
                auto to_interval = pair.second;
                int to_interval_reg = to_interval->get_assigned_reg();
                // find a register that is not used for assign other register later
                if(mov_map.find(to_interval_reg) == mov_map.end()){
                    auto from_phi_reg = LIRPhysicsRegister::create(from_interval->get_assigned_reg());
                    auto to_phi_reg = LIRPhysicsRegister::create(to_interval->get_assigned_reg());
                    auto mov_instr = LIRMovInst::create(to_phi_reg, from_phi_reg);
                    std::string mov_debug = "mov r" + std::to_string(to_interval->get_assigned_reg()) +
                                            ", r" + std::to_string(from_interval->get_assigned_reg());
                    mov_resolve.push_back(mov_debug);
                    if(insert_after_instr == nullptr){
                        insert_block->addInstFront(mov_instr);
                    }
                    else{
                        insert_block->addInstAfter(insert_after_instr, mov_instr);
                    }
                    insert_after_instr = mov_instr;
                    changed = true;
                    interval_map.erase(from_interval);
                    mov_map.erase(from_interval_reg);
                    break;
                }
            }
        }
        // third if interval_map is still not empty, wo need to handle loop
        while(!interval_map.empty()){
            for(auto pair: interval_map){
                // for any pair, we first remove it from interval_map
                auto from_interval = pair.first;
                int from_interval_reg = from_interval->get_assigned_reg();
                auto to_interval = pair.second;
                int to_interval_reg = to_interval->get_assigned_reg();
                mov_map.erase(from_interval_reg);
                interval_map.erase(from_interval);
                // then create a mov betweem tmp and from
                // mov tmp, from
                // then the loop is broken
                auto tmp_phi_reg = LIRPhysicsRegister::create(12);
                auto from_phi_reg = LIRPhysicsRegister::create(from_interval_reg);
                auto to_phi_reg = LIRPhysicsRegister::create(to_interval_reg);
                auto tmp_mov_instr = LIRMovInst::create(tmp_phi_reg, from_phi_reg);
                std::string mov_debug = "mov r" + std::to_string(12) +
                                        ", " + std::to_string(from_interval->get_assigned_reg());
                mov_resolve.push_back(mov_debug);
                if(insert_after_instr == nullptr){
                    insert_block->addInstFront(tmp_mov_instr);
                }
                else{
                    insert_block->addInstAfter(insert_after_instr, tmp_mov_instr);
                }
                insert_after_instr = tmp_mov_instr;
                changed = true;
                while(!interval_map.empty() && changed){
                    changed = false;
                    for(auto pair_: interval_map){
                        auto from_interval_ = pair_.first;
                        int from_interval_reg_ = from_interval_->get_assigned_reg();
                        auto to_interval_ = pair_.second;
                        int to_interval_reg_ = to_interval_->get_assigned_reg();
                        // find a register that is not used for assign other register later
                        if(mov_map.find(to_interval_reg_) == mov_map.end()){
                            auto from_phi_reg_ = LIRPhysicsRegister::create(from_interval_->get_assigned_reg());
                            auto to_phi_reg_ = LIRPhysicsRegister::create(to_interval_->get_assigned_reg());
                            auto mov_instr = LIRMovInst::create(to_phi_reg_, from_phi_reg_);
                            auto x = "mov r" + std::to_string(to_interval_->get_assigned_reg()) + ", r" +
                                     std::to_string(from_interval_->get_assigned_reg());
                            mov_resolve.push_back(x);
                            if(insert_after_instr == nullptr){
                                insert_block->addInstFront(mov_instr);
                            }
                            else{
                                insert_block->addInstAfter(insert_after_instr, mov_instr);
                            }
                            insert_after_instr = mov_instr;
                            changed = true;
                            interval_map.erase(from_interval_);
                            mov_map.erase(from_interval_reg_);
                            break;
                        }
                    }
                }
                // when while is end, we need to mov tmp to to_reg
                // mov to, tmp
                auto tmp_mov_back_instr = LIRMovInst::create(to_phi_reg, tmp_phi_reg);
                auto y = "mov r" + std::to_string(to_interval->get_assigned_reg()) + ", r" +
                         std::to_string(12);
                mov_resolve.push_back(y);
                insert_block->addInstAfter(insert_after_instr, tmp_mov_back_instr);
                insert_after_instr = tmp_mov_back_instr;
                break;
            }
        }
        std::cout << "-------- AFTER RESOLVE --------" << std::endl;
        for(auto s: store_resolve){
            std::cout << s << std::endl;
        }
        for(auto s: mov_resolve){
            std::cout << s << std::endl;
        }
        for(auto s: load_resolve){
            std::cout << s << std::endl;
        }
    }

};

#endif //BAZINGA_COMPILER_LINEARSCAN_H

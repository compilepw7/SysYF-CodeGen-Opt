#include <iostream>
#include "LinearScan.h"
#include "LIR/LIRInstruction.h"
#include "LIR/LIRLabel.h"

void LinearScan::computeBlockOrder() {
    struct WorkListElem {
        LIRBasicBlock *bb;
        bool operator<(const WorkListElem &rhs) const {
            return bb->getLoopDepth() < rhs.bb->getLoopDepth();
        }
        explicit WorkListElem(LIRBasicBlock *b) : bb(b) {}
    };
    std::priority_queue<WorkListElem> workList;
    // 初始化入边信息
    workList.push(WorkListElem(*_currentFunction->begin()));
    while (!workList.empty()) {
        LIRBasicBlock *bb = workList.top().bb;
        workList.pop();
        blocks.push_back(bb);
        for (auto nextBB: bb->getSuccessorBB()) {
            nextBB->decreaseForwardBranches();
            if (nextBB->isForwardBranchesEQZero()) {
                workList.push(WorkListElem(nextBB));
            }
        }
    }
}

void LinearScan::numberOperations() {
    int next_id = 0;
    for (auto bb : blocks) {
        BBfirst[bb] = next_id;
        for (auto op : *bb) {
            op->assignInstId(next_id);
            inst_map[next_id] = op;
            inst_map[next_id + 1] = op;
            next_id += 2;
        }
        BBLast[bb] = next_id - 2;
    }
}

void LinearScan::computeLocalLiveSets() {
    LIR_OpVisitState visitor;
    for (auto bb : blocks) {
        auto &live_gen = bb->getLiveGen();
        auto &live_kill = bb->getLiveKill();
        live_gen.clear();
        live_kill.clear();
        for (auto op : *bb) {
            visitor.visit(op);
            for (auto opr : visitor.input_oprs) {
                if (live_kill.find(opr) == live_kill.end()) {
                    live_gen.insert(opr);
                }
            }
            for (auto opr : visitor.temp_oprs) {
                live_kill.insert(opr);
            }
            for (auto opr : visitor.output_oprs) {
                live_kill.insert(opr);
            }
        }
    }
}

void LinearScan::computeGlobalLiveSets() {
    bool changed = true;
    do {
        changed = false;
        for (auto bb = blocks.rbegin(); bb != blocks.rend(); ++bb) {
            auto &live_out = (*bb)->getLiveOut();
            std::set<LIROperand *> liveOut;
            for (auto sux: (*bb)->getSuccessorBB()) {
                for (auto out: sux->getLiveIn()) {
                    liveOut.insert(out);
                }
            }
            // 判断两集合是否相等
            if (!changed) {
                for (auto it: liveOut) {
                    if (live_out.find(it) == live_out.end()) {
                        changed = true;
                        break;
                    }
                }
            }
            if (!changed) {
                for (auto it: live_out) {
                    if (liveOut.find(it) == liveOut.end()) {
                        changed = true;
                        break;
                    }
                }
            }
            live_out = liveOut;
            for (auto kill: (*bb)->getLiveKill()) {
                liveOut.erase(kill);
            }
            for (auto gen: (*bb)->getLiveGen()) {
                liveOut.insert(gen);
            }
            auto &live_in = (*bb)->getLiveIn();
            // 判断两集合是否相等
            if (!changed) {
                for (auto it: liveOut) {
                    if (live_in.find(it) == live_in.end()) {
                        changed = true;
                        break;
                    }
                }
            }
            if (!changed) {
                for (auto it: live_in) {
                    if (liveOut.find(it) == liveOut.end()) {
                        changed = true;
                        break;
                    }
                }
            }
            live_in = liveOut;
        }
    } while (changed);
}

void LinearScan::buildIntervals() {
    LIR_OpVisitState visitor;
    for (int i = 0; i < std::min(_currentFunction->getNumArgs(), 4); ++i) {
        addRangeOrCreate(LIRPhysicsRegister::create(i), -1, 2 * i);
    }
    for (auto rit = blocks.rbegin(); rit != blocks.rend(); ++rit) {
        if ((*rit)->getInstructions().empty()) continue;
        auto first_op = (*rit)->getInstructions().front();
        auto last_op = (*rit)->getInstructions().back();
        int block_from = first_op->getInstId();
        int block_to = last_op->getInstId() + 2;
        auto live_out = (*rit)->getLiveOut();

        for (auto opr : live_out) {
            addRangeOrCreate(opr, block_from, block_to);
        }

        for (auto rop = (*rit)->getInstructions().rbegin();
             rop != (*rit)->getInstructions().rend(); ++rop) {
            visitor.visit(*rop);

            // 特殊处理固定的寄存器
            if (auto br = dynamic_cast<LIRBranchInstruction *>(*rop)) {
                if (!br->isCall() && br->getNumOperands() > 0) {
                    /**
                     * 说明是有返回值return语句
                     * 被翻译为
                     *      OpId - 2: mov r0, vx
                     *      OpId + 0: b label_exit
                     * 将Range设置为 OpId-1 ~ OpID+1
                     */
                    addRangeOrCreate(LIRPhysicsRegister::create(0), br->getInstId() - 1, br->getInstId() + 1);
                    addUsePosition(LIRPhysicsRegister::create(0), br->getInstId() - 2, 1);
                    addUsePosition(LIRPhysicsRegister::create(0), br->getInstId(), 1);
                } else if (br->isCall()) {
                    /**
                     * 说明是call语句
                     * 被翻译为
                     * OpId - 8: mov r3, vx1
                     * OpId - 6: mov r2, vx2
                     * OpId - 4: mov r1, vx3
                     * OpId - 2: mov r0, vx3
                     * OpId + 0: bl label
                     * OpId + 2: mov vx4, r0 (If has return value)
                     */
                    auto target = dynamic_cast<LIRFunction *>(br->getTarget());
                    for (int i = 0; i < std::min(target->getNumArgs(), 4); ++i) {
                        // 因为load等语句的存在，需要向上扫描找到对应的mov的位置
                        auto it = rop;
                        while (!((*it)->getInstType() == LIRInstruction::LMovTy &&
                                 (*it)->getResult() == LIRPhysicsRegister::create(i)))
                            it++;
                        addRangeOrCreate(LIRPhysicsRegister::create(i), (*it)->getInstId(),
                                         (target->hasResult() && i == 0) ? br->getInstId() +
                                                                           (target->getNumArgs() > 4 ? 4 : 2)
                                                                         : br->getInstId());
                        addUsePosition(LIRPhysicsRegister::create(i), (*it)->getInstId(), 1);      // mov rx, vx
                        addUsePosition(LIRPhysicsRegister::create(i), br->getInstId(), 1);         // bl label
                    }
                    for (int i = target->getNumArgs(); i < 4; ++i) {
                        addRangeOrCreate(LIRPhysicsRegister::create(i), br->getInstId(), br->getInstId() + 1);
                        addUsePosition(LIRPhysicsRegister::create(i), br->getInstId(), 1);          // bl label
                    }
                    if (target->getNumArgs() == 0 && target->hasResult()) {
                        addRangeOrCreate(LIRPhysicsRegister::create(0), br->getInstId(), br->getInstId() + 2);
                        addUsePosition(LIRPhysicsRegister::create(0), br->getInstId() + 2, 1);  // mov rx, r0
                    }
                    if (target->getName() == "_sysy_starttime" || target->getName() == "_sysy_stoptime") {
                        addRangeOrCreate(LIRPhysicsRegister::create(0), br->getInstId() - 2, br->getInstId());
                    }
                }
            }

            for (auto opr : visitor.output_oprs) {
                //addRangeOrCreate(opr, (*rop)->getInstId(), block_to);
                if (intervals.find(opr) == intervals.end())
                    addRangeOrCreate(opr, (*rop)->getInstId(), block_to);
                else
                    intervals[opr]->get_range().front()->from = (*rop)->getInstId();
                addUsePosition(opr, (*rop)->getInstId(), opr->isPhysicsRegTy());
            }

            for (auto opr : visitor.temp_oprs) {
                addRangeOrCreate(opr, (*rop)->getInstId(), (*rop)->getInstId() + 1);
                addUsePosition(opr, (*rop)->getInstId(), opr->isPhysicsRegTy());
            }

            for (auto opr : visitor.input_oprs) {
                addRangeOrCreate(opr, block_from, (*rop)->getInstId());
                addUsePosition(opr, (*rop)->getInstId(), opr->isPhysicsRegTy());
            }

        }
    }
}

void LinearScan::resolveDataFlow() {
    MoveResolver resolver;
    for (auto bb : blocks) {
        for (auto succ : bb->getSuccessorBB()) {
            for (auto opr : succ->getLiveIn()) {
                int succ_first = BBfirst[succ];
                int bb_last = BBLast[bb];
                Interval *parent_interval = intervals[opr];
                Interval *from_interval = parent_interval->child_at(bb_last);
                Interval *to_interval = parent_interval->child_at(succ_first);
                if(from_interval->covers(succ_first))
                    continue;
                if(to_interval->covers(bb_last))
                    continue;
                if (from_interval->get_assigned_reg() != -1) {
                    if (from_interval->get_first_use_position() != INT32_MAX) {
                        if (inst_map[from_interval->get_first_use_position()]->getParent() !=
                            inst_map[from_interval->get_range().front()->from]->getParent() &&
                            from_interval->have_parent())
                            from_interval = from_interval->get_near();
                    }
                }

                if (from_interval != to_interval) {
                    std::cout << "Add interval:" << std::endl;
                    std::cout << bb->getLabelName() << ":  " << from_interval->print();
                    std::cout << succ->getLabelName() << ":  " << to_interval->print();
                    std::cout << std::endl;
                    resolver.add_mapping(from_interval, to_interval);
                }
            }
            resolver.find_insert_position(bb, succ);
            resolver.resolve_mappings();
        }
    }
}

void LinearScan::sillyWalkIntervals() {
    active.clear();
    inactive.clear();

    for (auto &interval : intervals) {
        if (interval.second->get_reg_num() == -1) {
            active.push_back(interval.second);
        } else {
            unhandled.push(interval.second);
        }
    }

    while (!unhandled.empty()) {
        auto current = unhandled.top();
        unhandled.pop();
        auto pos = current->get_range().front()->from;
        auto it = active.begin();
        while (it != active.end()) {
            if ((*it)->get_range().back()->to < pos) {
                handled.push_back(*it);
                it = active.erase(it);
            } else if (!(*it)->covers(pos)) {
                inactive.push_back(*it);
                it = active.erase(it);
            } else {
                it++;
            }
        }
        it = inactive.begin();
        while (it != inactive.end()) {
            if ((*it)->get_range().back()->to < pos) {
                handled.push_back(*it);
                it = inactive.erase(it);
            } else if ((*it)->covers(pos)) {
                active.push_back(*it);
                it = inactive.erase(it);
            } else {
                it++;
            }
        }

        if (!sillyTryAllocateFreeReg(current, pos)) {
            sillyHandleBlocked(current);
        }

        if (current->get_assigned_reg() != -1) {
            active.push_back(current);
        }
    }
}

void LinearScan::sillyHandleBlocked(Interval *current) {
    auto vReg = LIRVirtualRegister::getVReg(current->get_reg_num());
    int spillId = _currentFunction->allocStack(vReg, 1);
    current->setSpillId(spillId);
}

void LinearScan::sillyAssignRegNum() {
    _currentFunction->clear_load();
    LIR_OpVisitState visitor;
    // 再预留3个位置
    int pos = _currentFunction->allocStack(nullptr, 3);
    std::cout << "[D] Spill pos allocate at " << std::to_string(pos) << std::endl;
    for (auto bb : blocks) {
        std::map<LIRInstruction *, std::pair<std::list<LIRInstruction *>, std::list<LIRInstruction *>>> map;
        for (auto op : bb->getInstructions()) {
            std::list<LIRInstruction *> tbaF;
            std::list<LIRInstruction *> tbaA;
            int useStatus[20] = {0,};
            /**
             * 扫描占用的物理寄存器
             */
            for (int i = 0; i < op->getNumOperands(); ++i) {
                auto opr = op->getOperand(i);
                if (auto vr = dynamic_cast<LIRVirtualRegister *>(opr)) {
                    auto inv = intervals[vr];
                    if (inv->get_assigned_reg() == -1) {

                    } else {
                        op->setOperand(i, LIRPhysicsRegister::create(inv->get_assigned_reg()));
                        useStatus[inv->get_assigned_reg()] = 1;
                    }
                }
                if (auto pr = dynamic_cast<LIRPhysicsRegister *>(opr)) {
                    useStatus[pr->getRegisterID()] = 1;
                }
            }
            /**
             * 处理返回值分配到的寄存器
             */
            if (auto ret = dynamic_cast<LIRPhysicsRegister *>(op->getResult())) {
                useStatus[ret->getRegisterID()] = 1;
            } else if (auto ret = dynamic_cast<LIRVirtualRegister *>(op->getResult())) {
                auto intv = intervals[ret];
                if (intv->get_assigned_reg() != -1) {
                    useStatus[intv->get_assigned_reg()] = 1;
                }
            }
            /**
             * 分配并添加load store语句
             */
            for (int i = 0; i < op->getNumOperands(); ++i) {
                auto opr = op->getOperand(i);
                if (auto vr = dynamic_cast<LIRVirtualRegister *>(opr)) {
                    auto inv = intervals[vr];
                    if (inv->get_assigned_reg() == -1) {
                        int j = 10;
                        for (; j >= 0; --j) {
                            if (useStatus[j] == 0) break;
                        }
                        auto reg = LIRPhysicsRegister::create(j);
                        op->setOperand(i, reg);
                        _currentFunction->add_load(inv->getSpillId());
                        tbaF.push_back(LIRLoadInst::create(reg, LIRPhysicsRegister::create(13), LIRConstantOperand::create(inv->getSpillId())));
                        useStatus[j] = 2;
                    }
                }
            }
            /**
             * 设置结果
             */
            if (auto vReg = dynamic_cast<LIRVirtualRegister *>(op->getResult())) {
                auto inv = intervals[vReg];
                if (inv->get_assigned_reg() == -1) {
                    int j = 10;
                    for (; j >= 0; --j) {
                        if (useStatus[j] == 0) break;
                    }
                    auto reg = LIRPhysicsRegister::create(j);
                    _currentFunction->set_load(inv->getSpillId());
                    auto str = LIRStoreInst::create(reg, LIRPhysicsRegister::create(13), LIRConstantOperand::create(inv->getSpillId()));
                    op->setResult(reg);
                    tbaA.push_back(str);
                    useStatus[j] = 2;
                } else {
                    op->setResult(LIRPhysicsRegister::create(inv->get_assigned_reg()));
                }
            }
            /**
             * 添加 load/store 指令
             */
            int j = 0;
            for (int i = 0; i < 11; ++i) {
                if (useStatus[i] == 2) {
                    _currentFunction->add_load(pos + 4 * j);
                    _currentFunction->set_load(pos + 4 * j);
                    tbaA.push_back(LIRLoadInst::create(LIRPhysicsRegister::create(i), LIRPhysicsRegister::create(13), LIRConstantOperand::create(pos + 4 * j)));
                    tbaF.push_front(LIRStoreInst::create(LIRPhysicsRegister::create(i), LIRPhysicsRegister::create(13), LIRConstantOperand::create(pos + 4 * j)));
                    j += 1;
                }
            }
            map[op] = {tbaF, tbaA};
        }
        /**
         * 加入没营养的指令
         */
        for (const auto& pair: map) {
            for (auto inst: pair.second.first) {
                bb->addInstBeforeSimple(pair.first, inst);
            }
            for (auto inst = pair.second.second.rbegin(); inst != pair.second.second.rend(); ++inst) {
                bb->addInstAfter(pair.first, *inst);
            }
        }
    }
}

bool LinearScan::sillyTryAllocateFreeReg(Interval *current, int pos) {
    int free_pos[11];
    for (int &free_po : free_pos) {
        free_po = INT32_MAX;
    }
    for (auto it:active) {
        free_pos[it->get_assigned_reg()] = 0;
    }
    for (auto it:inactive) {
        if (it->intersects(current)) {
            int fp = it->next_intersection(current, pos);
            if (fp < free_pos[it->get_assigned_reg()])
                free_pos[it->get_assigned_reg()] = fp;
        }
    }
    int reg = 0;
    for (int i = 1; i <= 10; i++) {
        if (free_pos[i] > free_pos[reg]) {
            reg = i;
        }
    }

    if (free_pos[reg] == 0) {
        return false;
    } else if (free_pos[reg] > current->get_range().back()->to) {
        current->set_assigned_reg(reg);
        return true;
    } else {
        return false;
    }
}

bool LinearScan::tryAllocateFreeReg(Interval *current, int pos) {
    int free_pos[11];
    for (int &free_po : free_pos) {
        free_po = INT32_MAX;
    }
    for (auto it:active) {
        free_pos[it->get_assigned_reg()] = 0;
    }
    for (auto it:inactive) {
        if (it->intersects(current)) {
            int fp = it->next_intersection(current, pos);
            if (fp < free_pos[it->get_assigned_reg()])
                free_pos[it->get_assigned_reg()] = fp;
        }
    }
    int reg = 0;
    for (int i = 1; i <= 10; i++) {
        if (free_pos[i] > free_pos[reg]) {
            reg = i;
        }
    }

    if (free_pos[reg] == 0) {
        return false;
    } else if (free_pos[reg] > current->get_range().back()->to) {
        current->set_assigned_reg(reg);
        return true;
    } else {
        auto split_pos = free_pos[reg];
        if (current->get_range().front()->from == split_pos - 2)
            return false;
        current->set_assigned_reg(reg);
        auto split = current->split(split_pos - 2);
        if (split != nullptr) {
            intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
            unhandled.push(split);
        }
        return true;
    }
    return false;
}

void LinearScan::allocateBlockedReg(Interval *current, int pos) {
    int use_pos[11];
    int block_pos[11];
    for (int i = 0; i <= 10; i++) {
        use_pos[i] = INT32_MAX;
        block_pos[i] = INT32_MAX;
    }

    for (auto it : active) {
        if (it->get_reg_num()!=-1) {
            auto up = it->get_next_use_position(pos);
            if (up < use_pos[it->get_assigned_reg()])
                use_pos[it->get_assigned_reg()] = up;
        }
    }

    for (auto it : inactive) {
        if (it->get_reg_num()!=-1) {
            {
                auto up = it->get_next_use_position(pos);
                if (up < use_pos[it->get_assigned_reg()])
                    use_pos[it->get_assigned_reg()] = up;
            }
        }
    }

    for (auto it : active) {
        if (it->get_reg_num()==-1) {
            block_pos[it->get_assigned_reg()] = 0;
            use_pos[it->get_assigned_reg()] = 0;
        }
    }

    for (auto it : inactive) {
        if (it->get_reg_num()==-1) {
            if (it->intersects(current)) {
                auto bp = it->next_intersection(current, pos);
                if (bp < block_pos[it->get_assigned_reg()]) {
                    block_pos[it->get_assigned_reg()] = bp;
                    if (use_pos[it->get_assigned_reg()] > block_pos[it->get_assigned_reg()])
                        use_pos[it->get_assigned_reg()] = bp;
                }
            }
        }
    }

    std::set<int> bad_reg;
    int reg = 0;
    while (bad_reg.size() < 5) {
        reg = -1;
        int reg_pos = -1;
        for (int i = 0; i < 11; i++) {
            if (reg_pos < use_pos[i] && bad_reg.find(i) == bad_reg.end()) {
                reg = i;
                reg_pos = use_pos[i];
            }
        }
        if (block_pos[reg] < pos)
            bad_reg.insert(reg);
        else
            break;
    }
    assert(reg != -1);

    if (use_pos[reg] < current->get_first_use_position()) {
        /**
         * 发生Split操作
         * current的第一次使用位置比use_pos的最大值大，first_use前切分current，第一部分加入Spill
         * Case 1. 上一块就没有被分配寄存器，则该Interval继续保持在栈里，无需添加str操作
         * Case 2. 上一块被分配了寄存器，则在该Interval开始时插入str操作
         */
        int spill_pos = current->get_range().front()->from;
        int split_pos = current->get_first_use_position();
        auto split = current->split(split_pos - 2);
        int spillId = _currentFunction->allocStack(LIRVirtualRegister::getVReg(current->get_reg_num()), 1);
        current->setSpillId(spillId);
        if(current->have_parent()) {
            auto brother = current->get_brother(pos);
            if(brother->get_last_use_position()==-1)
                return;
            //if (brother->get_range().back()->to == current->get_range().front()->from) {
            auto store = LIRStoreInst::create(LIRPhysicsRegister::create(brother->get_assigned_reg()),
                                              LIRPhysicsRegister::create(13),
                                              LIRConstantOperand::create(spillId));
            auto inst = inst_map.at(brother->get_last_use_position());
            inst->getParent()->addInstAfter(inst, store);
            //}
        }
        if (split != nullptr) {
            intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
            unhandled.push(split);
        }
    } else if (block_pos[reg] > current->get_range().back()->to) {
        /**
         * 当前将要分配的寄存器没有被block，对非fixed的区间进行切分
         */
        current->set_assigned_reg(reg);
        for (auto it : active) {
            if (it->get_assigned_reg() == reg && it->intersects(current)) {
                /**
                 * All active and inactive intervals for this register intersecting with current are split before the
                 * start of current and spilled to the stack
                 */
                int spill_pos = current->get_range().front()->from;
                Interval *split;
                if(spill_pos <= it->get_range().front()->from){
                    split = it;
                }
                else{
                    split = it->split(spill_pos);
                }
                if (split != nullptr) {
                    // These split children are not considered during allocation
                    // any more because they do not have a register assigned
                    // If they have a use positions requiring a register, however,
                    // they must be reloaded again to a register later on.
                    // Therefore, they are split a second time before these use positions, and the second
                    // split children are sorted into the unhandled list
                    // Active 中的块一定会有寄存器被分配，直接插入 str 语句即可
                    auto stackPos = _currentFunction->allocStack(LIRVirtualRegister::getVReg(it->get_reg_num()), 1);
                    auto str = LIRStoreInst::create(LIRPhysicsRegister::create(it->get_assigned_reg()),
                                                    LIRPhysicsRegister::create(13),
                                                    LIRConstantOperand::create(stackPos));
                    inst_map.at(spill_pos)->getParent()->addInstBefore(inst_map.at(spill_pos), str);
                    intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
                    split->setSpillId(stackPos);
                    int nextUse = split->getNextUsePos(spill_pos);
                    if (nextUse != INT32_MAX) {
                        // 接下来仍然有UsePosition，继续切分
                        auto nu = split->split(nextUse - 2);
                        intervals[LIRVirtualRegister::getVReg(nu->get_reg_num())] = nu;
                        unhandled.push(nu);
                    }
                }
            }
        }
        for (auto it : inactive) {
            if (it->get_assigned_reg() == reg && it->intersects(current)) {
                //SPILL
                int spill_pos = 0;
                for (auto tmp : it->get_range()) {
                    if (tmp->to < pos && tmp->to > spill_pos) {
                        spill_pos = tmp->to;
                    }
                }
                //int spill_pos = current->get_range().front()->from;
                int split_pos = current->get_range().back()->to;
                auto split = it->split(split_pos);
                if (split != nullptr) {
                    // These split children are not considered during allocation
                    // any more because they do not have a register assigned
                    // If they have a use positions requiring a register, however,
                    // they must be reloaded again to a register later on.
                    // Therefore, they are split a second time before these use positions, and the second
                    // split children are sorted into the unhandled list
                    // inactive 中的块一定会有寄存器被分配，直接插入 str 语句即可，但是要在上一个区间的结束时插入
                    auto stackPos = _currentFunction->allocStack(LIRVirtualRegister::getVReg(it->get_reg_num()), 1);
                    auto str = LIRStoreInst::create(LIRPhysicsRegister::create(it->get_assigned_reg()),
                                                    LIRPhysicsRegister::create(13),
                                                    LIRConstantOperand::create(stackPos));
                    intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
                    inst_map.at(it->get_range().back()->to)->getParent()->addInstBefore(
                            inst_map.at(it->get_range().back()->to), str);
                    split->setSpillId(stackPos);
                    int nextUse = split->getNextUsePos(spill_pos);
                    if (nextUse != INT32_MAX) {
                        // 接下来仍然有UsePosition，继续切分
                        auto nu = split->split(nextUse - 2);
                        intervals[LIRVirtualRegister::getVReg(nu->get_reg_num())] = nu;
                        unhandled.push(nu);
                    }
                }
            }
        }
    } else {
        current->set_assigned_reg(reg);
        auto cur_split = current->split(block_pos[reg]);
        intervals[LIRVirtualRegister::getVReg(cur_split->get_reg_num())] = cur_split;
        unhandled.push(cur_split);
        for (auto it : active) {
            if (it->get_assigned_reg() == reg && it->intersects(current)) {
                /**
                 * All active and inactive intervals for this register intersecting with current are split before the
                 * start of current and spilled to the stack
                 */
                int spill_pos = current->get_range().front()->from;
                Interval *split;
                if(spill_pos <= it->get_range().front()->from){
                    split = it;
                }
                else{
                    split = it->split(spill_pos);
                }
                if (split != nullptr) {
                    // These split children are not considered during allocation
                    // any more because they do not have a register assigned
                    // If they have a use positions requiring a register, however,
                    // they must be reloaded again to a register later on.
                    // Therefore, they are split a second time before these use positions, and the second
                    // split children are sorted into the unhandled list
                    // Active 中的块一定会有寄存器被分配，直接插入 str 语句即可
                    auto stackPos = _currentFunction->allocStack(LIRVirtualRegister::getVReg(it->get_reg_num()), 1);
                    auto str = LIRStoreInst::create(LIRPhysicsRegister::create(it->get_assigned_reg()),
                                                    LIRPhysicsRegister::create(13),
                                                    LIRConstantOperand::create(stackPos));
                    inst_map.at(spill_pos)->getParent()->addInstBefore(inst_map.at(spill_pos), str);
                    //TODO
                    intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
                    split->setSpillId(stackPos);
                    int nextUse = split->getNextUsePos(spill_pos);
                    if (nextUse != INT32_MAX) {
                        // 接下来仍然有UsePosition，继续切分
                        auto nu = split->split(nextUse - 2);
                        intervals[LIRVirtualRegister::getVReg(nu->get_reg_num())] = nu;
                        unhandled.push(nu);
                    }
                }
            }
        }
        for (auto it : inactive) {
            if (it->get_assigned_reg() == reg && it->intersects(current)) {
                //SPILL
                int spill_pos = 0;
                for (auto tmp : it->get_range()) {
                    if (tmp->to < pos && tmp->to > spill_pos) {
                        spill_pos = tmp->to;
                    }
                }
                //int spill_pos = current->get_range().front()->from;
                int split_pos = current->get_range().back()->to;
                auto split = it->split(split_pos);
                if (split != nullptr) {
                    // These split children are not considered during allocation
                    // any more because they do not have a register assigned
                    // If they have a use positions requiring a register, however,
                    // they must be reloaded again to a register later on.
                    // Therefore, they are split a second time before these use positions, and the second
                    // split children are sorted into the unhandled list
                    // inactive 中的块一定会有寄存器被分配，直接插入 str 语句即可，但是要在上一个区间的结束时插入
                    auto stackPos = _currentFunction->allocStack(LIRVirtualRegister::getVReg(it->get_reg_num()), 1);
                    auto str = LIRStoreInst::create(LIRPhysicsRegister::create(it->get_assigned_reg()),
                                                    LIRPhysicsRegister::create(13),
                                                    LIRConstantOperand::create(stackPos));
                    // TODO: 使用 spill_pos 还是 it 的结尾 ?
                    intervals[LIRVirtualRegister::getVReg(split->get_reg_num())] = split;
                    inst_map.at(it->get_range().back()->to)->getParent()->addInstBefore(
                            inst_map.at(it->get_range().back()->to), str);
                    split->setSpillId(stackPos);
                    int nextUse = split->getNextUsePos(spill_pos);
                    if (nextUse != INT32_MAX) {
                        // 接下来仍然有UsePosition，继续切分
                        auto nu = split->split(nextUse - 2);
                        intervals[LIRVirtualRegister::getVReg(nu->get_reg_num())] = nu;
                        unhandled.push(nu);
                    }
                }
            }
        }
    }
}

void LinearScan::clearAll() {
    std::priority_queue<Interval *, std::vector<Interval *>, cmp> empty;
    std::swap(empty, unhandled);
    handled.clear();
    BBfirst.clear();
    BBLast.clear();
    active.clear();
    inactive.clear();
    blocks.clear();
    intervals.clear();
    inst_map.clear();
}

void LinearScan::addRangeOrCreate(LIROperand *opr, int from, int to) {
    assert(opr && !opr->isConstantTy() && "Constant type doesn't have a live interval.");
    int regId;
    if (opr->isPhysicsRegTy()) {
        regId = -1;
    } else {
        regId = dynamic_cast<LIRVirtualRegister *>(opr)->getRegisterID();
    }
    if (intervals.find(opr) == intervals.end()) {
        auto inv = new Interval(regId);
        if (regId == -1) {
            inv->set_assigned_reg(dynamic_cast<LIRPhysicsRegister *>(opr)->getRegisterID());
        }
        assert(from <= to && "from > to");
        inv->add_range(from, to);
        intervals[opr] = inv;
    } else {
        assert(from <= to && "from > to");
        intervals[opr]->add_range(from, to);
    }
}

void LinearScan::addUsePosition(LIROperand *opr, int pos, int kind) {
    assert(opr && !opr->isConstantTy() && "Constant type doesn't have a live interval.");
    assert(intervals.find(opr) != intervals.end() && "Can't find a live interval for operand");
    intervals[opr]->add_use_pos(pos, kind);
}

std::list<LIRInstruction *> LinearScan::range_move_instructions(std::list<LIRInstruction *> mov_list) {
    std::set<LIRMovInst *> mov_instr_set;
    std::list<LIRInstruction *> return_list;
    std::map<int, int> mov_map;
    for(auto instr: mov_list){
        auto mov_instr = dynamic_cast<LIRMovInst *>(instr);
        assert(mov_instr && "want get mov instr but get other inistr!");
        mov_instr_set.insert(mov_instr);
        auto from = mov_instr->getOperand(0);
        auto to = mov_instr->getResult();
        int from_reg = intervals[from]->get_assigned_reg();
        int to_reg = intervals[to]->get_assigned_reg();
        mov_map[from_reg] = to_reg;
    }
    bool changed = true;
    while(!mov_instr_set.empty() && changed){
        changed = false;
        for(auto mov_instr: mov_instr_set){
            auto from = mov_instr->getOperand(0);
            auto to = mov_instr->getResult();
            int from_reg = intervals[from]->get_assigned_reg();
            int to_reg = intervals[to]->get_assigned_reg();
            // if to_reg is not assigned to other reg later
            if(mov_map.find(to_reg) == mov_map.end()){
                return_list.push_back(mov_instr);
                changed = true;
                mov_instr_set.erase(mov_instr);
                break;
            }
        }
    }
    // if mov_instr_set is still not empty, that means there are loop remained
    while(!mov_instr_set.empty()){
        for(auto mov_instr: mov_instr_set){
            auto from = mov_instr->getOperand(0);
            auto to = mov_instr->getResult();
            int from_reg = intervals[from]->get_assigned_reg();
            int to_reg = intervals[to]->get_assigned_reg();
            mov_instr_set.erase(mov_instr);
            mov_map.erase(from_reg);
            auto from_phi_reg_ = LIRPhysicsRegister::create(from_reg);
            auto temp_phi_reg = LIRPhysicsRegister::create(12);
            auto to_phi_reg_ = LIRPhysicsRegister::create(to_reg);
            auto temp_mov_out_instr = LIRMovInst::create(temp_phi_reg, from_phi_reg_);
            return_list.push_back(temp_mov_out_instr);
            changed = true;
            while(!mov_instr_set.empty() && changed){
                changed = false;
                for(auto mov_instr_: mov_instr_set){
                    auto from_ = mov_instr_->getOperand(0);
                    auto to_ = mov_instr_->getResult();
                    int from_reg_ = intervals[from_]->get_assigned_reg();
                    int to_reg_ = intervals[to_]->get_assigned_reg();
                    if(mov_map.find(to_reg_) == mov_map.end()){
                        return_list.push_back(mov_instr_);
                        mov_instr_set.erase(mov_instr_);
                        mov_map.erase(from_reg_);
                        changed = true;
                        break;
                    }
                }
            }
            auto temp_mov_in_instr = LIRMovInst::create(to_phi_reg_, temp_phi_reg);
            return_list.push_back(temp_mov_in_instr);
        }
    }
    return return_list;
}

void LinearScan::sillyLinearScan(LIRFunction *f) {
    clearAll();
    _currentFunction = f;

    // order blocks and operations (including loop detection)
    computeBlockOrder();
    numberOperations();
    // create intervals with live ranges
    computeLocalLiveSets();
    computeGlobalLiveSets();
    buildIntervals();

    // allocate registers
    sillyWalkIntervals();

    // replace virtual registers with physical registers
    sillyAssignRegNum();
}

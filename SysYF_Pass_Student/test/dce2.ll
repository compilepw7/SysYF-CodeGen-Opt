declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define i32 @main() {
label_entry:
  %op0 = alloca i32
  %op1 = alloca i32
  store i32 1, i32* %op1
  %op2 = alloca i32
  store i32 2, i32* %op2
  %op3 = alloca i32
  %op4 = load i32, i32* %op1
  %op5 = load i32, i32* %op2
  %op6 = add i32 %op4, %op5
  store i32 %op6, i32* %op3
  %op7 = alloca i32
  %op8 = load i32, i32* %op1
  %op9 = load i32, i32* %op2
  %op10 = add i32 %op8, %op9
  store i32 %op10, i32* %op7
  %op11 = alloca i32
  %op12 = load i32, i32* %op1
  %op13 = load i32, i32* %op2
  %op14 = add i32 %op12, %op13
  %op15 = add i32 %op14, 3
  store i32 %op15, i32* %op11
  %op16 = load i32, i32* %op11
  store i32 %op16, i32* %op0
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  %op17 = load i32, i32* %op0
  ret i32 %op17
}

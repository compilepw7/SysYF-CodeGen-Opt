declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define i32 @main() {
label_entry:
  %op10 = add i32 1, 2
  %op11 = add i32 %op10, 3
  %op13 = add i32 1, 1
  %op17 = add i32 %op13, 2
  %op18 = add i32 %op17, 3
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  ret i32 %op18
}

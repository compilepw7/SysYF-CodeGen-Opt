@a = global i32 1
@b = global i32 2
@c = global i32 zeroinitializer
@d = global i32 zeroinitializer
declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define void @main() {
label_entry:
  %op0 = load i32, i32* @a
  %op1 = load i32, i32* @b
  %op2 = add i32 %op0, %op1
  store i32 %op2, i32* @c
  %op3 = load i32, i32* @a
  %op4 = load i32, i32* @b
  %op5 = add i32 %op3, %op4
  store i32 %op5, i32* @d
  %op6 = load i32, i32* @c
  %op7 = load i32, i32* @d
  %op8 = add i32 %op6, %op7
  store i32 %op8, i32* @a
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  ret void
}

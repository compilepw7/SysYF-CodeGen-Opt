@a = global [4 x i32] [i32 0, i32 1, i32 2, i32 3]
declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define i32 @main() {
label_entry:
  %op2 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 0
  %op3 = load i32, i32* %op2
  %op6 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 %op3
  %op7 = load i32, i32* %op6
  %op10 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 %op3
  %op11 = load i32, i32* %op10
  %op12 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 %op11
  %op13 = load i32, i32* %op12
  %op15 = add i32 %op3, 1
  %op17 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 %op15
  %op18 = load i32, i32* %op17
  %op26 = add i32 %op7, %op18
  %op27 = srem i32 %op26, 4
  %op28 = getelementptr [4 x i32], [4 x i32]* @a, i32 0, i32 %op27
  %op29 = load i32, i32* %op28
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  ret i32 %op18
}

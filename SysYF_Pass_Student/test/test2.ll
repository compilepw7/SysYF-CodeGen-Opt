@a = global i32 zeroinitializer
@b = global i32 zeroinitializer
@c = global i32 zeroinitializer
@n = global i32 0
declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define void @main() {
label_entry:
  store i32 10395, i32* @a
  %op0 = load i32, i32* @a
  %op1 = load i32, i32* @a
  %op2 = mul i32 %op0, %op1
  %op3 = sitofp i32 %op2 to float
  %op4 = fmul float %op3, 0x3fce0418a0000000
  %op5 = fmul float %op4, 0x3fce0418a0000000
  %op6 = fptosi float %op5 to i32
  store i32 %op6, i32* @b
  %op7 = load i32, i32* @a
  %op8 = load i32, i32* @a
  %op9 = mul i32 %op7, %op8
  %op10 = sitofp i32 %op9 to float
  %op11 = fmul float %op10, 0x3fce0418a0000000
  %op12 = fmul float %op11, 0x3fce0418a0000000
  %op13 = fptosi float %op12 to i32
  store i32 %op13, i32* @c
  %op14 = alloca i32
  %op15 = alloca i32
  %op16 = alloca i32
  %op17 = alloca i32
  %op18 = alloca i32
  %op19 = load i32, i32* @a
  %op20 = load i32, i32* @a
  %op21 = mul i32 %op19, %op20
  %op22 = load i32, i32* @a
  %op23 = mul i32 %op21, %op22
  store i32 %op23, i32* %op14
  %op24 = load i32, i32* %op14
  %op25 = load i32, i32* %op14
  %op26 = mul i32 %op24, %op25
  %op27 = load i32, i32* %op14
  %op28 = mul i32 %op26, %op27
  store i32 %op28, i32* %op15
  %op29 = load i32, i32* %op15
  %op30 = load i32, i32* %op15
  %op31 = mul i32 %op29, %op30
  %op32 = load i32, i32* %op15
  %op33 = mul i32 %op31, %op32
  store i32 %op33, i32* %op17
  %op34 = load i32, i32* %op17
  %op35 = load i32, i32* %op17
  %op36 = mul i32 %op34, %op35
  %op37 = load i32, i32* %op17
  %op38 = mul i32 %op36, %op37
  store i32 %op38, i32* %op18
  %op39 = load i32, i32* %op18
  %op40 = load i32, i32* %op18
  %op41 = mul i32 %op39, %op40
  %op42 = load i32, i32* %op18
  %op43 = mul i32 %op41, %op42
  store i32 %op43, i32* %op16
  %op44 = load i32, i32* %op16
  %op45 = load i32, i32* %op16
  %op46 = mul i32 %op44, %op45
  %op47 = load i32, i32* %op16
  %op48 = mul i32 %op46, %op47
  store i32 %op48, i32* @n
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  ret void
}

@a = global i32 zeroinitializer
@b = global i32 zeroinitializer
@c = global i32 zeroinitializer
@n = global i32 0
declare i32 @get_int()

declare float @get_float()

declare i32 @get_char()

declare i32 @get_int_array(i32*)

declare i32 @get_float_array(float*)

declare void @put_int(i32)

declare void @put_float(float)

declare void @put_char(i32)

declare void @put_int_array(i32, i32*)

declare void @put_float_array(i32, float*)

define void @main() {
label_entry:
  store i32 10395, i32* @a
  %op0 = load i32, i32* @a
  %op1 = load i32, i32* @a
  %op2 = mul i32 %op0, %op1
  %op3 = sitofp i32 %op2 to float
  %op4 = fmul float %op3, 0x3fce0418a0000000
  %op5 = fmul float %op4, 0x3fce0418a0000000
  %op6 = fptosi float %op5 to i32
  store i32 %op6, i32* @b
  %op7 = load i32, i32* @a
  %op8 = load i32, i32* @a
  %op9 = mul i32 %op7, %op8
  %op10 = sitofp i32 %op9 to float
  %op11 = fmul float %op10, 0x3fce0418a0000000
  %op12 = fmul float %op11, 0x3fce0418a0000000
  %op13 = fptosi float %op12 to i32
  store i32 %op13, i32* @c
  %op19 = load i32, i32* @a
  %op20 = load i32, i32* @a
  %op21 = mul i32 %op19, %op20
  %op22 = load i32, i32* @a
  %op23 = mul i32 %op21, %op22
  %op26 = mul i32 %op23, %op23
  %op28 = mul i32 %op26, %op23
  %op31 = mul i32 %op28, %op28
  %op33 = mul i32 %op31, %op28
  %op36 = mul i32 %op33, %op33
  %op38 = mul i32 %op36, %op33
  %op41 = mul i32 %op38, %op38
  %op43 = mul i32 %op41, %op38
  %op46 = mul i32 %op43, %op43
  %op48 = mul i32 %op46, %op43
  store i32 %op48, i32* @n
  br label %label_ret
label_ret:                                                ; preds = %label_entry
  ret void
}

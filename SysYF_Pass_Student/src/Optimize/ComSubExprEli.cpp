#include "Pass.h"
#include "ComSubExprEli.h"
#include <set>
#include <algorithm>

void ComSubExprEli::execute()
{
    module->set_print_name();
    for (auto func : module->get_functions())
    {
        if (func->get_basic_blocks().empty()) //空块继续
            continue;
        for (auto block : func->get_basic_blocks())
        {
            def.clear();
            gen[block] = {};
            kill[block] = {};                           //从0开始
            for (auto Inst : block->get_instructions()) //第一次遍历 生成allinstructioninblocks
            {
                if (Inst->is_add() || Inst->is_mul() || Inst->is_sub() || Inst->is_div() || Inst->is_rem())
                {
                    SubExpr *t = new SubExpr(Inst);
                    bool is_in = false;
                    for (SubExpr *every : all)
                    {
                        if (*every == *t)
                        {
                            is_in = true;
                            break;
                        }
                    }
                    if (!is_in)
                        all.push_back(t);
                    def.push_back(Inst);
                }
            }
            int def_index = -1;
            bool redefed;
            for (auto Inst : block->get_instructions())
            {
                if (Inst->is_add() || Inst->is_mul() || Inst->is_sub() || Inst->is_div() || Inst->is_rem())
                {
                    SubExpr *t = new SubExpr(Inst);
                    def_index++;
                    redefed = false;
                    for (SubExpr *every : all)
                    {
                        if (*every == *t)
                        {
                            for (int k = def_index + 1; k < def.size(); k++)
                            {
                                if (def[k] == every->ins)
                                {
                                    redefed = true;
                                    break;
                                }
                            }
                            if (!redefed)
                                gen[block].insert(every);
                            break;
                        }
                    }
                    for (SubExpr *every : all)
                    {
                        if (Inst == every->op1 || Inst == every->op2)
                            kill[block].insert(every);
                    }
                }
            }
        }
        int bit_vector_size = all.size();
        for (auto block : func->get_basic_blocks())
        {
            gen_e[block] = BitVector(bit_vector_size, false);
            kill_e[block] = BitVector(bit_vector_size, false);
            for (SubExpr *expr : gen[block])
            {
                int i = -1;
                for (SubExpr *find : all)
                {
                    i++;
                    if (*expr == *find)
                    {
                        gen_e[block].set(i, 1);
                        break;
                    }
                }
            }
            for (SubExpr *expr : kill[block])
            {
                int i = -1;
                for (SubExpr *find : all)
                {
                    i++;
                    if (*expr == *find)
                    {
                        kill_e[block].set(i, 1);
                        break;
                    }
                }
            }
        }
        bool entry = true;
        for (auto block : func->get_basic_blocks())
        {
            if (entry)
            {
                out[block] = BitVector(bit_vector_size, false);
                entry = false;
            }
            else
                out[block] = BitVector(bit_vector_size, true);
        }
        bool changed = true;
        while (changed)
        {
            changed = false;
            entry = true;
            for (auto block : func->get_basic_blocks())
            {
                if (entry)
                {
                    entry = false;
                    continue;
                }
                BitVector tmp = BitVector(bit_vector_size, true);
                for (auto blocksuccessor : block->get_succ_basic_blocks())
                {
                    for (int i = 0; i < bit_vector_size; i++)
                    {
                        tmp.set(i, tmp.get(i) || out[blocksuccessor].get(i));
                    }
                }
                in[block] = tmp;
                for (int i = 0; i < bit_vector_size; i++)
                {
                    tmp.set(i, tmp.get(i) && !kill_e[block].get(i) && gen_e[block].get(i));
                }
                if (!(tmp == out[block]))
                {
                    changed = true;
                    out[block] = tmp;
                }
            }
        }
//         这里是我们自己写的方法，复杂度较高
        for(auto bb: func->get_basic_blocks()){
            std::vector<Value *> newlis;
            for(int i=0;i<in[bb].getsize();i++){
                if(in[bb].get(i)==1){
                    newlis.push_back(all[i]->ins);
                }
            }
            for(auto ins : bb->get_instructions()) {
                Value * ins_tmp = ins;
                std::set<Value*> ops;
                for(auto op : ins->get_operands())
                    ops.insert(op);
                bool is_find = false;
                Instruction * to_del;
                for(auto t : newlis){
                    Instruction * tt = dynamic_cast<Instruction *>(t);
                    std::set<Value*> ops1;
                    for(auto op : tt->get_operands())
                        ops1.insert(op);
                    if(ops1 == ops){
                        is_find = true;
                        to_del = tt;
                        break;
                    }
                }
                if(!is_find)
                    newlis.push_back(ins);
                else {
                    for(auto use: to_del->get_use_list()){
                        auto use_ins = dynamic_cast<Instruction *>(use.val_);
                        use_ins->set_operand(use.arg_no_, ins);
                    }
                    bb->delete_instr(dynamic_cast<Instruction *>(to_del));
                }
            }
        }

//        // 这里是询问同学后获得的复杂度较低的方法
//        std::map<std::set<Value*>,std::vector<Value*>> ins_with_same_ops;
//        std::map<BasicBlock*,std::set<Instruction*>> In;
//        std::map<BasicBlock*,std::set<Instruction*>> Out;
//        for(auto bb: func->get_basic_blocks()) {
//            std::set<Instruction*> x;
//            for (int i = 0; i < in[bb].getsize(); i++) {
//                if (in[bb].get(i) == 1) {
//                    x.insert(dynamic_cast<Instruction *>(all[i]->ins));
//                }
//            }
//            std::set<Instruction*> y;
//            for (int i = 0; i < out[bb].getsize(); i++) {
//                if (out[bb].get(i) == 1) {
//                    y.insert(dynamic_cast<Instruction *>(all[i]->ins));
//                }
//            }
//
//            In.insert(std::make_pair(bb, x));
//            Out.insert(std::make_pair(bb, y));
//        }
//
//        for(auto bb: func->get_basic_blocks()){
//            for(auto ins: Out[bb]){
//                std::set<Value*> ops;
//                for(auto op : ins->get_operands())
//                    ops.insert(op);
//                ins_with_same_ops[ops].push_back(ins);
//            }
//        }
//        for(auto bb:func->get_basic_blocks()){
//            for(auto ins: Out[bb]){
//                std::set<Value*> ops;
//                for(auto op : ins->get_operands())
//                    ops.insert(op);
//                auto first_ins = ins_with_same_ops[ops][0];
//                for(auto all_ins: ins_with_same_ops[ops]){
//                    if(all_ins!=first_ins){
//                        if(bb->find_instruction(dynamic_cast<Instruction *>(all_ins))!=bb->get_instructions().end()){
//                            for(auto use: all_ins->get_use_list()){
//                                auto use_ins = dynamic_cast<Instruction *>(use.val_);
//                                use_ins->set_operand(use.arg_no_, first_ins);
//                            }
//                            bb->delete_instr(dynamic_cast<Instruction *>(all_ins));
//                        }
//                    }
//                }
//            }
//        }
    }


}
bool ComSubExprEli::is_valid_expr(Instruction *inst)
{
    return !(inst->is_void() || inst->is_call() || inst->is_phi() || inst->is_alloca() || inst->is_load() || inst->is_cmp() || inst->is_zext()); //TODO:CHECK VALID INST
}
/**
 * 有In/Out之后怎么办
 * for bb in range fuc->bb
 *      newlis = in[bb] subexpr
 *      for inst in bb->inst
 *           if *inst in newlis:
 *              全局替换 *inst -> old
 *              del *inst 一行
 *           else:
 *              newlist.append(inst) 
 *          
 * 
 * **/

#include "ActiveVar.h"

#include <algorithm>

bool ActiveVar::is_defined(Value* val, BasicBlock* block) {
    return def_list[block].find(val) != def_list[block].end();
}
bool ActiveVar::is_const(Value* val) {
    return dynamic_cast<ConstantInt*>(val) || dynamic_cast<ConstantFloat*>(val);
}
bool ActiveVar::is_live_at_entry(Value* val, BasicBlock* block) {
    return block->get_live_in().find(val) != block->get_live_in().end();
}
bool ActiveVar::is_live_at_exit(Value* val, BasicBlock* block) {
    return block->get_live_out().find(val) != block->get_live_out().end();
}

void ActiveVar::execute() {

    use_list.clear();
    def_list.clear();
    activated_vars.clear();
    for (auto &func : this->module->get_functions()) {
        if (func->get_basic_blocks().empty()) {
            continue;
        } 
        else {
            func_ = func;  
            
            /*you need to finish this function*/
            for (BasicBlock* block : func_->get_basic_blocks()) {
                for (Instruction* instruction : block->get_instructions()) {
                    if (instruction->is_add() || instruction->is_sub() || instruction->is_mul() || instruction->is_div() || instruction->is_rem()) {
                        Value* lval = instruction->get_operand(0);
                        if (!is_const(lval) && !is_defined(lval, block)) {
                            use_list[block].insert(lval);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(lval);
                            }
                        }
                        Value* rval = instruction->get_operand(1);
                        if (!is_const(rval) && !is_defined(rval, block)) {
                            use_list[block].insert(rval);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(rval);
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_cmp()) {
                        Value* lval = instruction->get_operand(0);
                        if (!is_const(lval) && !is_defined(lval, block)) {
                            use_list[block].insert(lval);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(lval);
                            }
                        }
                        Value* rval = instruction->get_operand(1);
                        if (!is_const(rval) && !is_defined(rval, block)) {
                            use_list[block].insert(rval);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(rval);
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_zext()) {
                        Value* opd = instruction->get_operand(0);
                        if (def_list[block].find(opd) == def_list[block].end()) {
                            use_list[block].insert(opd);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(opd);
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_call()) {
                        for (int i = 1; i < instruction->get_num_operand(); i++) {
                            Value* arg = instruction->get_operand(i);
                            if (!is_const(arg) && !is_defined(arg, block)) {
                                use_list[block].insert(arg);
                                for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                    activated_vars[prev].insert(arg);
                                }
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_ret()) {
                        if (instruction->get_num_operand()) {
                            Value* opd = instruction->get_operand(0);
                            if (!is_const(opd) && !is_defined(opd, block)) {
                                use_list[block].insert(opd);
                                for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                    activated_vars[prev].insert(opd);
                                }
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_phi()) {
                        for (int i = 0; i < instruction->get_num_operand(); i += 2) {
                            Value* opd = instruction->get_operand(i);
                            if (!is_const(opd) && !is_defined(opd, block)) {
                                use_list[block].insert(opd);
                                for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                    if (prev->get_name() == instruction->get_operand(i + 1)->get_name()) activated_vars[prev].insert(opd);
                                }
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_load()) {
                        Value* opd = instruction->get_operand(0);
                        if (!is_const(opd) && !is_defined(opd, block)) {
                            use_list[block].insert(opd);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(opd);
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_gep()) {
                        for (int i = 0; i < instruction->get_num_operand(); i++) {
                            Value* opd = instruction->get_operand(i);
                            if (!is_const(opd) && !is_defined(opd, block)) {
                                use_list[block].insert(opd);
                                for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                    activated_vars[prev].insert(opd);
                                }
                            }
                        }
                        def_list[block].insert(instruction);
                    }
                    else if (instruction->is_alloca()) {
                        def_list[block].insert(instruction);
                    }
                }
            }

            bool is_changed = true;
            while (is_changed) {
                is_changed = false;
                for (BasicBlock* block : func_->get_basic_blocks()) {
                    for (BasicBlock* succ : block->get_succ_basic_blocks()) {
                        for (Value* var : succ->get_live_in()) {
                            if (!is_live_at_exit(var, block) && (activated_vars[block].find(var) != activated_vars[block].end())) {
                                block->get_live_out().insert(var);
                            }
                        }
                    }
                    for (Value* var : use_list[block]) {
                        if (!is_live_at_entry(var, block)) {
                            is_changed = true;
                            block->get_live_in().insert(var);
                        }
                    }
                    for (Value* var : block->get_live_out()) {
                        if (!is_defined(var, block) && !is_live_at_entry(var, block)) {
                            is_changed = true;
                            block->get_live_in().insert(var);
                            for (BasicBlock* prev : block->get_pre_basic_blocks()) {
                                activated_vars[prev].insert(var);
                            }
                        }
                    }
                }
            }
        }
    }
    return ;
}
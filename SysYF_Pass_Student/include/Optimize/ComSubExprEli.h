#ifndef SYSYF_COMSUBEXPRELI_H
#define SYSYF_COMSUBEXPRELI_H

#include "Pass.h"
#include <map>
#include <set>
#include <vector>
#include "Instruction.h"
#include <cstddef>
/*****************************CommonSubExprElimination**************************************/
/***************************This class is based on SSA form*********************************/
/***************************you need to finish this class***********************************/

class SubExpr
{
public:
    int Optype;
    Value *op1, *op2, *ins;
    // bool isUnary;

    SubExpr(Instruction *i)
    {
        ins = i;
        if (i->is_add())
            Optype = 0;
        else if (i->is_mul())
            Optype = 1;
        else if (i->is_sub())
            Optype = 2;
        else if (i->is_div())
            Optype = 3;
        else if (i->is_rem())
            Optype = 4;
        else
        {
            std::cout << "Unexpected type when handling subexpr." << std::endl;
            abort();
        }
        op1 = i->get_operand(0);
        op2 = i->get_operand(1);
    }
    bool operator==(const SubExpr a)
    {
        if (a.ins == this->ins)
            return true;
        if (a.Optype != this->Optype)
            return false;
        if (a.Optype == 0 || a.Optype == 1)
            return ((a.op1 == this->op1 && a.op2 == this->op2) || (a.op1 == this->op2 && a.op2 == this->op1));
        else
            return (a.op1 == this->op1 && a.op2 == this->op2);
    }
};

class BitVector
{
public:
    typedef bool BitValType;
    // static const BitValType TRUE = true;
    // static const BitValType FALSE = false;
    BitVector()
        : BitVector(0)
    {
    }
    BitVector(int size, bool init_fill = true)
    {
        int n = size;
        this->data_ = new int[n];
        if (!init_fill)
        {
            for (int i = 0; i < n; i++)
            {
                this->data_[i] = 0;
            }
        }
        else
        {
            for (int i = 0; i < n; i++)
            {
                this->data_[i] = 1;
            }
        }
        this->size_ = size;
    }
    ~BitVector()
    {
        // delete[] this->data_;
    }
    int &get(int index)
    {
        if (index < 0 || index > this->size_)
        {
            throw std::out_of_range("BitVector::get() parameter index");
            // return BitVector::FALSE;
        }
        return this->data_[index];
    }
    bool set(int index, const BitValType &val)
    {
        if (index < 0 || index > this->size_)
        {
            throw std::out_of_range("BitVector::set() parameter index");
            // return false;
        }
        this->data_[index] = val;
        return true;
    }
    bool operator==(BitVector a)
    {
        if (a.getsize() != this->getsize())
        {
            return false;
        }
        else
        {
            for (int i = 0; i < a.getsize(); i++)
            {
                if (a.get(i) != this->get(i))
                    return false;
            }
            return true;
        }
    }
    int getsize()
    {
        return size_;
    }

private:
    int size_;
    int *data_;
};

class ComSubExprEli : public Pass
{
public:
    explicit ComSubExprEli(Module *module) : Pass(module) {}
    const std::string get_name() const override { return name; }
    void execute() override;
    static bool is_valid_expr(Instruction *inst);
    std::map<BasicBlock *, std::set<SubExpr *>> gen, kill;
    std::vector<SubExpr *> all;
    std::vector<Value *> def;
    std::map<BasicBlock *, BitVector> in, out, gen_e, kill_e;

private:
    const std::string name = "ComSubExprEli";
};

#endif // SYSYF_COMSUBEXPRELI_H
#ifndef SYSYF_ACTIVEVAR_H
#define SYSYF_ACTIVEVAR_H

#include "Pass.h"
#include "Module.h"

class ActiveVar : public Pass
{
public:
    ActiveVar(Module *module) : Pass(module) {}
    void execute() final;
    const std::string get_name() const override {return name;}
private:
    Function *func_;
    const std::string name = "ActiveVar";

    std::map<BasicBlock*, std::set<Value*>> activated_vars;

    std::map<BasicBlock*, std::set<Value*>> use_list;

    std::map<BasicBlock*, std::set<Value*>> def_list;

    bool is_defined(Value* val, BasicBlock* block);
    bool is_const(Value* val);
    bool is_live_at_entry(Value* val, BasicBlock* block);
    bool is_live_at_exit(Value* val, BasicBlock* block);
};

#endif  // SYSYF_ACTIVEVAR_H
